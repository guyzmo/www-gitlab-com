---
layout: markdown_page
title: Organizational Structure
page_class: org-structure
---

- [General](/handbook/) - [CEO](/jobs/chief-executive-officer/)
  - [Marketing](/handbook/marketing/) - [CMO](/jobs/chief-marketing-officer/)
    - Design - Designer
    - Demand generation - Senior Demand Generation Manager
      - Online marketing - Online Marketing Manager
      - Business development
  - Sales - CRO
    - Account Executives
    - EMEA - Sales Director EMEA
      - Account Executives
    - APAC - Director of Global Alliances and APAC Sales
    - Customer Success - Customer Success Manager
      - Account Managers
      - Solution Engineers
  - Finance - CFO
  - Operations - COO
    - Support - Lead Service Engineer
      - Service Engineers
  - Technical - CTO
  - Engineering - VP of Engineering
    - Various teams - Various team leads
      - Developers
    - Design - Frontend Lead
      - UX Designers
      - Frontend engineers
    - Infrastructure - Senior Production Engineer
      - Production Engineers
      - Developers
  - General Product - VP of Product
  - CI/CD Product - Head of Product
  - People Operations - People Operations Manager
    - Administration - Administrative Coordinator
    - People Operations - People Operations Administrator
